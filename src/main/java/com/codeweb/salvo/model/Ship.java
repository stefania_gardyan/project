package com.codeweb.salvo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;


@Entity
public class Ship {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private int shipId;
    private String shipType;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "gameplayer_id")
    private GamePlayer gameplayer;

    @ElementCollection
    @Column(name = "locations")
    private List<String> locations;

    public Ship() {}
    public Ship(String shipType, List<String> locations) {
        this.shipType = shipType;
        this.locations = locations;
    }

    public long getShipId() {
        return shipId; }

    public String getShipType() {
        return shipType; }

    public void setShipType(String type) {
        this.shipType = shipType;
    }

    @JsonIgnore
    public GamePlayer getGameplayer() { return gameplayer; }
    public void setGameplayer(GamePlayer gameplayer) { this.gameplayer = gameplayer; }

    public List<String> getLocations() { return locations; }
    public void setLocations(List<String> locations) { this.locations = locations; }



}



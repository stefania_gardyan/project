package com.codeweb.salvo.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Player{

    @Id
    @GeneratedValue (strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;
    private String  name;
    private String email;

    @OneToMany (mappedBy = "player", fetch = FetchType.EAGER)
    private Set <GamePlayer>gamePlayers;
    public Player() {

    }
    public Player(String name, String email) {
        this.name = name;
        this.email = email;

    }
    public long getId() {return id;}

    public void setId(int id) { this.id = id;}

    public String getName () { return name;}

    public void setName (String name) { this.name = name;}

    public void setEmail (String email) {this.email = email;}

    public String getEmail() {
        return email;
    }

    public Set<GamePlayer> getGamePlayer () {
        return gamePlayers;
    }

    public void setGamePlayer(Set<GamePlayer> gamePlayer ) {
        this.gamePlayers = gamePlayers;

    }

}

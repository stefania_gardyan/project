package com.codeweb.salvo.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
public class Game{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;
    private Date creationDate;

    @OneToMany(mappedBy = "game", fetch = FetchType.EAGER)
    private Set<GamePlayer> gamePlayers;

    public Game() {
        this.creationDate = new Date();
    }

    public long getId() {
        return id;

    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;

    }

    public Set<GamePlayer> getGamePlayer() {
        return gamePlayers;
    }

    public void setGamePlayer(Set<GamePlayer> gamePlayer) {
        this.gamePlayers = gamePlayers;

    }

}

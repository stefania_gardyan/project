package com.codeweb.salvo.controller;


import com.codeweb.salvo.dto.GamePlayerDto;
import com.codeweb.salvo.model.Game;
import com.codeweb.salvo.model.GamePlayer;
import com.codeweb.salvo.model.Player;
import com.codeweb.salvo.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.codeweb.salvo.dto.GameDto.makeDTO;

@RestController
@RequestMapping("/api")
public class SalvoController {

    @Autowired
    GameRepository gameRepository;
    @Autowired
    GamePlayerRepository gamePlayerRepository;
    @Autowired
    PlayerRepository playerRepository;
    @Autowired
    SalvoRepository salvoRepository;
    @Autowired
    ShipRepository shipRepository;

    @RequestMapping("/games")
    public List<Map<String, Object>> getGameAll() {
        return gameRepository.findAll()
                .stream()
                .map(game -> makeDTO(game))
                .collect(Collectors.toList());
    }

    @RequestMapping("/game_view/{id}")
    public Map<String, Object> getGameView(@PathVariable Long id) {
        GamePlayer gp_rep = gamePlayerRepository.findById(id).get();
        return GamePlayerDto.gameView(gp_rep);

    }
}
package com.codeweb.salvo;


import com.codeweb.salvo.model.Game;
import com.codeweb.salvo.model.GamePlayer;
import com.codeweb.salvo.model.Player;
import com.codeweb.salvo.model.Ship;
import com.codeweb.salvo.repository.ShipRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import com.codeweb.salvo.repository.GamePlayerRepository;
import com.codeweb.salvo.repository.GameRepository;
import com.codeweb.salvo.repository.PlayerRepository;

import java.util.Arrays;
import java.util.Date;

@SpringBootApplication
public class SalvoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SalvoApplication.class, args);


	}

	@Bean
	public CommandLineRunner initData(PlayerRepository playerRepository,
									  GameRepository gameRepository,
									  GamePlayerRepository gamePlayerRepository,
									  ShipRepository repositoryShip) {
		return (args) -> {

			Player player1 = new Player("David","david@gmail.com");
			Player player2 = new Player("Rocket","rocket@gmail.com");

			playerRepository.save(player1);
			playerRepository.save(player2);

			Game game1 = new Game();
			Game game2 = new Game();
			game2.setCreationDate(Date.from(game1.getCreationDate().toInstant().plusSeconds(3600)));
			Game game3 = new Game();
			game3.setCreationDate(Date.from(game1.getCreationDate().toInstant().plusSeconds(7200)));

			gameRepository.save(game1);
			gameRepository.save(game2);
			gameRepository.save(game3);

			GamePlayer gamePlayer1 = new GamePlayer(player1,game1);
			GamePlayer gamePlayer2 = new GamePlayer(player2,game1);




			Ship dest1 = new Ship("Destructor", Arrays.asList("A1", "A2", "A3"));
			Ship subm1 = new Ship("Submarine", Arrays.asList("B1","B2","B3"));
			Ship patr1 = new Ship("Patrol_Boat", Arrays.asList("C1","C2"));
			Ship dest2 = new Ship("Destructor", Arrays.asList("D1", "D2", "D3"));
			Ship subm2 = new Ship("Submarine", Arrays.asList("E1","E2","E3"));
			Ship patr2 = new Ship("Patrol_Boat", Arrays.asList("F1","F2"));
			repositoryShip.save(dest1);
			repositoryShip.save(subm1);
			repositoryShip.save(patr1);
			repositoryShip.save(dest2);
			repositoryShip.save(subm2);
			repositoryShip.save(patr2);

			gamePlayer1.addShip(dest1);
				gamePlayerRepository.save(gamePlayer1);
			gamePlayer2.addShip(subm2);
				gamePlayerRepository.save(gamePlayer2);
			repositoryShip.save(dest1);
			repositoryShip.save(subm2);

		};





	}





}

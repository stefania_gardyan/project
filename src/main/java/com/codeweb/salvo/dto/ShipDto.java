package com.codeweb.salvo.dto;

import com.codeweb.salvo.model.Ship;

        import java.util.LinkedHashMap;
        import java.util.Map;

public class ShipDto {
    public static Map<String, Object> makeDTO(Ship ship){
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", ship.getShipId());
        dto.put("type", ship.getShipType());
        dto.put("locations", ship.getLocations());
        return dto;
    }
}
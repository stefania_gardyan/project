package com.codeweb.salvo.dto;

import com.codeweb.salvo.model.Salvo;

import java.util.LinkedHashMap;
import java.util.Map;

public class SalvoDto {
    public static Map<String, Object> makeDTO(Salvo salvo){
        Map<String, Object> dto = new LinkedHashMap<>();
        // dto.put("id", salvo.getId());
        dto.put("turn", salvo.getTurn());
        dto.put("locations", salvo.getLocations());
        return dto;
    }
}
package com.codeweb.salvo.dto;

import com.codeweb.salvo.model.Game;

import java.util.LinkedHashMap;
import java.util.Map;

import static java.util.stream.Collectors.toList;

public class GameDto {
    public static Map<String, Object> makeDTO(Game game){
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", game.getId());
        dto.put("created", game.getCreationDate());
        dto.put("gamePlayers", game.getGamePlayer().stream()
                .map(gp -> GamePlayerDto.makeDTO(gp))
                .collect(toList()));
        return dto;
    }
}
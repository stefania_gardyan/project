package com.codeweb.salvo.dto;

import com.codeweb.salvo.model.Game;
import com.codeweb.salvo.model.GamePlayer;

import java.util.LinkedHashMap;
import java.util.Map;

import static java.util.stream.Collectors.toList;

public class GamePlayerDto {
    public static Map<String, Object> makeDTO(GamePlayer gamePlayer){
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", gamePlayer.getId());
        dto.put("player", PlayerDto.makeDTO(gamePlayer.getPlayer()));
        return dto;
    }
    public static Map<String, Object> gameView(GamePlayer gamePlayer){
        Map<String, Object> dto = GameDto.makeDTO(gamePlayer.getGame());
        dto.put("ships", gamePlayer.getShips().stream()
                .map(s -> ShipDto.makeDTO(s))
                .collect(toList()));
        return dto;
    }
    public static void gameFullView(GamePlayer gamePlayer){
        Game game = gamePlayer.getGame();
        Map<String, Object> dto = GameDto.makeDTO(game);
        dto.put("ships", gamePlayer.getShips().stream()
                .map(s -> ShipDto.makeDTO(s))
                .collect(toList()));


        }

    }

